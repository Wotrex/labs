﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace lab08
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }


        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Припинити роботу застосунку?","Припинити роботу", MessageBoxButtons.OKCancel,MessageBoxIcon.Question) == DialogResult.OK)
                Application.Exit(); 
        }

        private void fMain_Load(object sender, EventArgs e)
        {
            gvBooks.AutoGenerateColumns = false;
            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Name";
            column.Name = "Назва книги";
            gvBooks.Columns.Add(column);  
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Author";
            column.Name = "Автор";
            gvBooks.Columns.Add(column); 
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Genre";
            column.Name = "Жанр";
            gvBooks.Columns.Add(column); 
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Copies";
            column.Name = "Кількість копій";
            gvBooks.Columns.Add(column); 
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Pages";
            column.Name = "Кількість сторінок";
            gvBooks.Columns.Add(column); 
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Cost";
            column.Name = "Ціна";
            gvBooks.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Fans";
            column.Name = "Кількість фанів";
            gvBooks.Columns.Add(column);
            EventArgs args = new EventArgs();
            OnResize(args);
        }
        private void btnBook_Click(object sender, EventArgs e)
        {
            Book book = new Book();
            fBook fb = new fBook(book);
            if (fb.ShowDialog() == DialogResult.OK)
            {
                bindingSource1.Add(book);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Book book = (Book)bindingSource1.List[bindingSource1.Position]; 
            fBook fb = new fBook(book);
            if (fb.ShowDialog() == DialogResult.OK)
            {
                bindingSource1.List[bindingSource1.Position] = book;
            } 
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Видалити поточний запис?", "Видалення запису", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                bindingSource1.RemoveCurrent();
            } 
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Очистити таблицю?\n\nВсі дані будуть втрачені","Очищення даних", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                bindingSource1.Clear();
            } 
        }

        private void btnExit_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("Закрити застосунок?", "Вихід з програми", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                Application.Exit(); 
            }


        }

    }
}
