﻿namespace lab08
{
    partial class fBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbBook = new System.Windows.Forms.GroupBox();
            this.tbFans = new System.Windows.Forms.TextBox();
            this.tbCost = new System.Windows.Forms.TextBox();
            this.tbPages = new System.Windows.Forms.TextBox();
            this.tbCopies = new System.Windows.Forms.TextBox();
            this.tbAuthor = new System.Windows.Forms.TextBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tbGenre = new System.Windows.Forms.ComboBox();
            this.gbBook.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbBook
            // 
            this.gbBook.Controls.Add(this.tbGenre);
            this.gbBook.Controls.Add(this.tbFans);
            this.gbBook.Controls.Add(this.tbCost);
            this.gbBook.Controls.Add(this.tbPages);
            this.gbBook.Controls.Add(this.tbCopies);
            this.gbBook.Controls.Add(this.tbAuthor);
            this.gbBook.Controls.Add(this.tbName);
            this.gbBook.Controls.Add(this.label7);
            this.gbBook.Controls.Add(this.label6);
            this.gbBook.Controls.Add(this.label5);
            this.gbBook.Controls.Add(this.label3);
            this.gbBook.Controls.Add(this.label4);
            this.gbBook.Controls.Add(this.label2);
            this.gbBook.Controls.Add(this.label1);
            this.gbBook.Location = new System.Drawing.Point(28, 24);
            this.gbBook.Name = "gbBook";
            this.gbBook.Size = new System.Drawing.Size(342, 335);
            this.gbBook.TabIndex = 0;
            this.gbBook.TabStop = false;
            this.gbBook.Text = "Загальні дані";
            // 
            // tbFans
            // 
            this.tbFans.Location = new System.Drawing.Point(175, 267);
            this.tbFans.Name = "tbFans";
            this.tbFans.Size = new System.Drawing.Size(100, 20);
            this.tbFans.TabIndex = 13;
            // 
            // tbCost
            // 
            this.tbCost.Location = new System.Drawing.Point(175, 231);
            this.tbCost.Name = "tbCost";
            this.tbCost.Size = new System.Drawing.Size(100, 20);
            this.tbCost.TabIndex = 12;
            // 
            // tbPages
            // 
            this.tbPages.Location = new System.Drawing.Point(175, 193);
            this.tbPages.Name = "tbPages";
            this.tbPages.Size = new System.Drawing.Size(100, 20);
            this.tbPages.TabIndex = 11;
            // 
            // tbCopies
            // 
            this.tbCopies.Location = new System.Drawing.Point(175, 155);
            this.tbCopies.Name = "tbCopies";
            this.tbCopies.Size = new System.Drawing.Size(100, 20);
            this.tbCopies.TabIndex = 10;
            // 
            // tbAuthor
            // 
            this.tbAuthor.Location = new System.Drawing.Point(175, 70);
            this.tbAuthor.Name = "tbAuthor";
            this.tbAuthor.Size = new System.Drawing.Size(100, 20);
            this.tbAuthor.TabIndex = 8;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(175, 32);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(100, 20);
            this.tbName.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(37, 270);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Кількість фанів";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(37, 234);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Ціна";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 196);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Кількість сторінок";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Жанр";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Кількість копій";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Автор";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Назва книги";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(396, 49);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(396, 87);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Скасувати";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // tbGenre
            // 
            this.tbGenre.FormattingEnabled = true;
            this.tbGenre.Items.AddRange(new object[] {
            "Авангардная литература",
            "Автобиография",
            "Архитектура и искусство",
            "Астрология и эзотерика",
            "Бизнес и финансы",
            "Боевик",
            "Воспитание и образование",
            "Детектив",
            "Дом, сад, огород",
            "Здоровье",
            "Исторический роман",
            "История",
            "Карьера",
            "Компьютеры",
            "Любовный роман",
            "Любовь и семейные отношения",
            "Мистика",
            "Мода и красота",
            "Музыка, кино, радио",
            "Наука и техника",
            "Питание и кулинария",
            "Приключения",
            "Политика, экономика, право",
            "Религия",
            "Саморазвитие и психология",
            "Спорт",
            "Триллер/ужасы",
            "Фантастика",
            "Фэнтези/сказки",
            "Философия",
            "Хобби",
            "Школьный учебник"});
            this.tbGenre.Location = new System.Drawing.Point(175, 111);
            this.tbGenre.Name = "tbGenre";
            this.tbGenre.Size = new System.Drawing.Size(100, 21);
            this.tbGenre.TabIndex = 14;
            // 
            // fBook
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(498, 386);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.gbBook);
            this.MaximizeBox = false;
            this.Name = "fBook";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Дані про нову книгу";
            this.Load += new System.EventHandler(this.fBook_Load);
            this.gbBook.ResumeLayout(false);
            this.gbBook.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbBook;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbFans;
        private System.Windows.Forms.TextBox tbCost;
        private System.Windows.Forms.TextBox tbPages;
        private System.Windows.Forms.TextBox tbCopies;
        private System.Windows.Forms.TextBox tbAuthor;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox tbGenre;
    }
}