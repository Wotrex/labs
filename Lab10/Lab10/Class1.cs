﻿using System;

public class Book
{
    public string Name { get; set; }
    public string Author { get; set; }
    public string Genre { get; set; }
    public int Copies { get; set; }
    public int Pages { get; set; }
    public double Cost { get; set; }
    public int Fans { get; set; }
    public double GetYearIncomePerInhabitant()
    {
        return Copies * Cost;
    }
    public Book()
    {
    }
    public Book(string name, string author, string genre, int copies, int pages, double cost, int fans) 
    {
        Name = name;
        Author = author;
        Genre = genre;
        Copies = copies;
        Pages = pages;
        Cost = cost;
        Fans = fans;
    }

}
