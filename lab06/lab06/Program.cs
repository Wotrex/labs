﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lib06;
namespace Lab06
{
    class Program
    {
        static void Main(string[] args)
        {
            Book[] arrBooks;
            Console.Write("Введiть кількість книг: ");
            int cntBooks = int.Parse(Console.ReadLine());
            arrBooks = new Book[cntBooks];
            for (int i = 0; i < cntBooks; i++)
            {
                Console.Write("Введiть назву книги: ");
                string sName = Console.ReadLine();
                Console.Write("Введiть iм'я автора: ");
                string sAuthor = Console.ReadLine();
                Console.Write("Введiть жанр: ");
                string sGenre = Console.ReadLine();
                Console.Write("Введiть кiлькiсть копiй: ");
                string sCopies = Console.ReadLine();
                Console.Write("Введiть кiлькiсть фанатiв: ");
                string sFans = Console.ReadLine();
                Console.Write("Введiть кiлькiсть сторiнок: ");
                string sPages = Console.ReadLine();
                Console.Write("Введiть цiну: ");
                string sCost = Console.ReadLine();
                Console.WriteLine();

                Console.WriteLine();
                Book TheBook = new Book();
                TheBook.Name = sName;
                TheBook.Author = sAuthor;
                TheBook.Genre = sGenre;
                TheBook.Copies = int.Parse(sCopies);
                TheBook.Fans = int.Parse(sFans);
                TheBook.Pages = int.Parse(sPages);
                TheBook.Cost = double.Parse(sCost);
                arrBooks[i] = TheBook;
            }
            foreach (Book b in arrBooks)
            {
                Console.WriteLine();
                Console.WriteLine("------------------------------------------------");
                Console.WriteLine("Данi про книгу: ");
                Console.WriteLine("------------------------------------------------");
                Console.WriteLine("Назва: " + b.Name);
                Console.WriteLine("Автор: " + b.Author);
                Console.WriteLine("Жанр: " + b.Genre);
                Console.WriteLine("Кiлькiсть копiй: " + b.Copies.ToString());
                Console.WriteLine("Кiлькiсть фанатiв: " + b.Fans.ToString());
                Console.WriteLine("Кiлькiсть сторiнок: " + b.Pages.ToString());
                Console.WriteLine("Можливий дохід: " + b.GetYearIncomePerInhabitant().ToString("0.00"));
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}