﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lib06
{
        public class Book
        {
            public string Name;
            public string Author;
            public string Genre;
            public int Copies;
            public int Pages;
            public double Cost;
            public int Fans;
            public double GetYearIncomePerInhabitant()
            {
                return Copies * Cost;
            }
        }
}
