﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace lab13
{
    public partial class fBook : Form
    {
        public Book TheBook;
        public fBook(Book b)
        {
            TheBook = b;
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            TheBook.Name = tbName.Text.Trim();
            TheBook.Author = tbAuthor.Text.Trim();
            TheBook.Genre = tbGenre.Text.Trim();
            TheBook.Copies = int.Parse(tbCopies.Text.Trim());
            TheBook.Pages = int.Parse(tbPages.Text.Trim());
            TheBook.Cost = double.Parse(tbCost.Text.Trim());
            TheBook.Fans = int.Parse(tbFans.Text.Trim());
            DialogResult = DialogResult.OK; 

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void fBook_Load(object sender, EventArgs e)
        {
            if (TheBook != null)
            {
                tbName.Text = TheBook.Name;
                tbAuthor.Text = TheBook.Author;
                tbGenre.Text = TheBook.Genre;
                tbCopies.Text = TheBook.Copies.ToString();
                tbPages.Text = TheBook.Pages.ToString();
                tbCost.Text = TheBook.Cost.ToString("0.00");
                tbFans.Text = TheBook.Fans.ToString();
            } 
        }
    }
}
