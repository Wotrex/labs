﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace lab13
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }


        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Припинити роботу застосунку?", "Припинити роботу", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                Application.Exit();
        }

        private void fMain_Load(object sender, EventArgs e)
        {
            gvBooks.AutoGenerateColumns = false;
            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Name";
            column.Name = "Назва книги";
            gvBooks.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Author";
            column.Name = "Автор";
            gvBooks.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Genre";
            column.Name = "Жанр";
            gvBooks.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Copies";
            column.Name = "Кількість копій";
            gvBooks.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Pages";
            column.Name = "Кількість сторінок";
            gvBooks.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Cost";
            column.Name = "Ціна";
            gvBooks.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Fans";
            column.Name = "Кількість фанів";
            gvBooks.Columns.Add(column);
            EventArgs args = new EventArgs();
            OnResize(args);


        }
        private void btnBook_Click(object sender, EventArgs e)
        {
            Book book = new Book();
            fBook fb = new fBook(book);
            if (fb.ShowDialog() == DialogResult.OK)
            {
                bindingSource1.Add(book);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Book book = (Book)bindingSource1.List[bindingSource1.Position];
            fBook fb = new fBook(book);
            if (fb.ShowDialog() == DialogResult.OK)
            {
                bindingSource1.List[bindingSource1.Position] = book;
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Видалити поточний запис?", "Видалення запису", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                bindingSource1.RemoveCurrent();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Очистити таблицю?\n\nВсі дані будуть втрачені", "Очищення даних", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                bindingSource1.Clear();
            }
        }

        private void btnExit_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("Закрити застосунок?", "Вихід з програми", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                Application.Exit();
            }


        }
        private void fMain_Resize(object sender, EventArgs e)
        {
            int buttonsSize = 9 * btnBook.Width + 3 * toolStripSeparator1.Width;
            btnExit.Margin = new Padding(Width - buttonsSize, 0, 0, 0); 
        }

        private void btnSaveAsText_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "Текстові файли (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.Title = "Зберегти дані у текстовомуформаті"; 
            saveFileDialog.InitialDirectory =Application.StartupPath;
            StreamWriter sw;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                sw = new StreamWriter(saveFileDialog.FileName, false, Encoding.UTF8);
                try
                {
                    foreach (Book book in bindingSource1.List)
                    {
                        sw.Write(book.Name + "\t" + book.Author + "\t" + book.Genre + "\t" + book.Copies + "\t" + book.Pages + "\t" + book.Cost + "\t" + book.Fans + "\t\n");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Сталась помилка: \n{0}", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    sw.Close();
                }
            }
        }

        private void btnSaveAsBinary_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "Файли даних (*.books)|*.books|All files (*.*)|*.*";
            saveFileDialog.Title = "Зберегти дані у бінарному форматі";
            saveFileDialog.InitialDirectory = Application.StartupPath;
            BinaryWriter bw;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                bw = new
               BinaryWriter(saveFileDialog.OpenFile()); try
                {
                    foreach (Book book in bindingSource1.List)
                    {
                        bw.Write(book.Name);
                        bw.Write(book.Author);
                        bw.Write(book.Genre);
                        bw.Write(book.Copies);
                        bw.Write(book.Pages);
                        bw.Write(book.Cost);
                        bw.Write(book.Fans);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Сталась помилка: \n{0}", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    bw.Close();
                }
            }
        }

        private void btnOpenFromText_Click(object sender, EventArgs e)
        {
            openFileDialog.Filter = "Текстові файли (*.txt)|*.txt|All files(*.*)|*.*";
            openFileDialog.Title = "Прочитати дані у текстовому форматі";
            openFileDialog.InitialDirectory = Application.StartupPath;
            StreamReader sr;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                bindingSource1.Clear();
                sr = new StreamReader(openFileDialog.FileName, Encoding.UTF8);
                string s;
                try
                {
                    while ((s = sr.ReadLine()) != null)
                    {
                        string[] split = s.Split('\t');
                        Book book = new Book(split[0], split[1], split[2], int.Parse(split[3]), int.Parse(split[4]), double.Parse(split[5]), int.Parse(split[6])); 
                        bindingSource1.Add(book);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Сталась помилка: \n{0}", ex.Message,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    sr.Close();
                }
            }
        }

        private void btnOpenFromBinary_Click(object sender, EventArgs e)
        {
            openFileDialog.Filter = "Файли даних (*.books)|*.books|All files (*.*)|*.*";
            openFileDialog.Title = "Прочитати дані у бінарному форматі";
            openFileDialog.InitialDirectory = Application.StartupPath;
            BinaryReader br;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                bindingSource1.Clear();
                br = new BinaryReader(openFileDialog.OpenFile());
                try
                {
                    Book book;
                    while (br.BaseStream.Position < br.BaseStream.Length)
                    {
                        book = new Book();
                        for (int i = 1; i <= 8; i++)
                        {
                            switch (i)
                            {
                                case 1:
                                    book.Name = br.ReadString();
                                    break;
                                case 2:
                                    book.Author = br.ReadString();
                                    break;
                                case 3:
                                    book.Genre = br.ReadString();
                                    break;
                                case 4:
                                    book.Copies = br.ReadInt32();
                                    break;
                                case 5:
                                    book.Pages = br.ReadInt32();
                                    break;
                                case 6:
                                    book.Cost = br.ReadDouble();
                                    break;
                                case 7:
                                    book.Fans = br.ReadInt32();
                                    break;
                            }
                        }
                        bindingSource1.Add(book);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Сталась помилка: \n{0}", ex.Message,
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    br.Close();
                }
            }
        }
    }
}
