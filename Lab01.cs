﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введiть початкове значення Xmin: ");
            string sxMin = Console.ReadLine();
            double xMin = Double.Parse(sxMin);

            Console.Write("Введiть кiнцеве значення Xmax: ");
            string sxMax = Console.ReadLine();
            double xMax = double.Parse(sxMax);

            Console.Write("Введiть прирiст dX: ");
            string sdx = Console.ReadLine();
            double dx = double.Parse(sdx);

            double x = xMin;
            double y;
            double s = 1;

            while (x <= xMax)
            {
                double x1 = x;
                double x2 = x * 3;
                double a = x1 + x2 + Math.Sin(x1 * x2);
                double b = 5 * Math.Cos(Math.Pow(x2, 2));
                y = Math.Sqrt(56 + (a/b));

                if (x > xMin && x < xMax)
                {
                    s = s * y;
                }
                Console.WriteLine("x = {0}\t\t y = {1}", x, y);

                x += dx;
            }
            if (Math.Abs(x - xMax - dx) > 0.0001)
            {
                y = Math.Pow(xMax, 2);
                Console.WriteLine("x = {0}\t\t y = {1}", xMax, y);
            }
            Console.WriteLine("s = s * y", s);
            Console.ReadKey();

        }
    }
}
