The program searches for duplicate files and displays them.
The program compares files by their name and size.
These files can be deleted in the program itself.
First you need to select the directory with the files.
To do this, click on the "..." button.
To find duplicate files, click the "Search" button.
To delete a duplicate, select it in the list and click the "Delete" button.
In order to save information about duplicate files, click the "Save" button.
To exit the program, click the "Exit" button.


�������� ���� �������� ����� � ������ ��. 
�������� ������� ����� �� �� ���� � ������.
ֳ ����� ����� ������ � ���� �������.
��� ������� ��� ������� ������� ��������� � ������ �������.
��� ����� ��������� �� ������ "...".
��� ���� ��� ������ �������� �����, ��������� ������ "�����".
��� ���� ��� �������� �������, ������� ���� � ������ � ��������� ������ "�������".
��� ���� ��� �������� ���������� ��� �������� �����, ��������� ������ "���������".
��� ���� ��� ����� � ��������, ��������� ������ "�����".



��������� ���� ��������� ������ � ���������� ��.
��������� ���������� ����� �� �� �������� � �������.
��� ����� ����� ������� � ����� ���������.
��� ������ ��� ����� ������� ���������� � �������.
��� ����� ������� �� ������ "...".
��� ���� ����� ����� ��������� ������, ������� ������ "�����".
��� ���� ����� ������� ��������, �������� ��� � ������ � ������� ������ "�������".
��� ���� ���� ��������� ���������� � ���������� ������, ������� ������ "���������".
��� ���� ����� ����� �� ���������, ������� ������ "�����".