﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        FolderBrowserDialog fbd = new FolderBrowserDialog();
        string[] files_list;
        string[] files;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                files_list = Directory.GetFiles(fbd.SelectedPath).Select(Path.GetFileNameWithoutExtension).Select(p => p.Substring(0)).ToArray();
                files = Directory.GetFiles(fbd.SelectedPath);
                listBox1.Items.Clear();
                listBox1.Items.AddRange(files_list);
                listBox2.Items.Clear();
                listBox3.Items.Clear();
                listBox3.Items.AddRange(files);
                textBox2.Text = fbd.SelectedPath.ToString();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Закрыть програму?", "Закрыть програму?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (files_list == null)
            {
                MessageBox.Show("Укажите директорию!", "Не указана директория", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                listBox2.Items.Clear();
                for (int i = 0; i < listBox1.Items.Count; i++)
                {
                    for (int c = 0; c < listBox1.Items.Count; c++)
                    {
                        if (i != c)
                        {
                            string a = listBox1.Items[i].ToString();
                            string b = listBox1.Items[c].ToString();
                            string a1 = listBox3.Items[i].ToString();
                            string b1 = listBox3.Items[c].ToString();
                            FileStream fs1 = new FileStream(a1, FileMode.Open);
                            FileStream fs2 = new FileStream(b1, FileMode.Open);
                            if (fs1.Length == fs2.Length && (a.Contains(b) || b.Contains(a)))
                            {
                                if (!listBox2.Items.Contains(a))
                                    {
                                        listBox2.Items.Add(a);
                                    }
                                if (!listBox2.Items.Contains(b))
                                {
                                    listBox2.Items.Add(b);
                                }
                            }
                            fs1.Close();
                            fs2.Close();
                        }
                        
                    }
                }
                if (listBox2.Items.Count == 0)
                {
                    MessageBox.Show("Дубликаты не найдены", "Дубликаты не найдены", MessageBoxButtons.OK);
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (files_list == null)
            {
                MessageBox.Show("Укажите директорию!", "Не указана директория", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (listBox2.SelectedItems.Count != 0) 
                {
                    if (MessageBox.Show("Вы точно хотите удалить файл?", "Удалить файл", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        MessageBox.Show("Файл " + listBox2.SelectedItem.ToString() + " удалён с вашего комап'ютера", "Файл удален", MessageBoxButtons.OK);
                        File.Delete(@"" + listBox2.SelectedItem.ToString());
                        listBox1.Items.RemoveAt(listBox2.SelectedIndex);
                        listBox2.Items.RemoveAt(listBox2.SelectedIndex);
                    }
                } else {
                    MessageBox.Show("Выберите файл дубликат!", "Файл не выбран", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            } 
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                if (listBox2.Items.Count != 0)
                {
                    File.WriteAllText(@"" + fbd.SelectedPath.ToString() + "/InfAboutDub.txt", listBox2.Items[0].ToString() + Environment.NewLine);
                    for (int h = 1; h < listBox2.Items.Count; h++)
                    {
                        File.AppendAllText(@"" + fbd.SelectedPath.ToString() + "/InfAboutDub.txt", listBox2.Items[h].ToString() + Environment.NewLine);
                    }
                        MessageBox.Show("Информация про дубликаты сохранена", "Сохранено", MessageBoxButtons.OK);
                }
            }
        }
    }
}
