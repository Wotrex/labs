﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace lab12
{
    public partial class fMain : Form
    {
        CEmblem[] emblems;
        int FiguresCount = 0;
        int CurrentFigureIndex;
        public fMain()
        {
            InitializeComponent();
            emblems = new CEmblem[100];
            cbFigureType.SelectedIndex = 0;
        }

        private void btnCreateNew_Click(object sender, EventArgs e)
        {
            if (FiguresCount >= 99)
            {
                MessageBox.Show("Досягнуто межі кількості об'єктів!");
                return;
            }
            Graphics graphics = pnMain.CreateGraphics();
            CurrentFigureIndex = FiguresCount;
            // Створення нового об'єкта - фігури
            if (cbFigureType.SelectedIndex == 0) // Створюємо коло
            {
                emblems[CurrentFigureIndex] =
                new CCircle(graphics, pnMain.Width / 2, pnMain.Height / 2,
                50);
                cbCircles.Items.Add("Фігура №" + (FiguresCount).ToString() +
                " [коло]");
            }
            else if (cbFigureType.SelectedIndex == 1)
            {
                emblems[CurrentFigureIndex] = new CRectangle(graphics,
               pnMain.Width / 2, pnMain.Height / 2,50);
                cbCircles.Items.Add("Фігура №" + (FiguresCount).ToString() +
                " [Ромб]");
            }
            else if (cbFigureType.SelectedIndex == 2)
            {
                emblems[CurrentFigureIndex] = new Rhombus(graphics,
               pnMain.Width / 2, pnMain.Height / 2, 50);
                cbCircles.Items.Add("Фігура №" + (FiguresCount).ToString() +
                " [Ромб Маленький]");
            }
            emblems[CurrentFigureIndex].Show();
            FiguresCount++;
            cbCircles.SelectedIndex = FiguresCount - 1;
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) ||
             (CurrentFigureIndex < 0))
                return;
            emblems[CurrentFigureIndex].Hide();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) ||
             (CurrentFigureIndex < 0))
                return;
            emblems[CurrentFigureIndex].Show();
        }

        private void btnExpand_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) ||
             (CurrentFigureIndex < 0))
                return;
            emblems[CurrentFigureIndex].Expand(5);
        }

        private void btnCollapse_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) ||
             (CurrentFigureIndex < 0))
                return;
            emblems[CurrentFigureIndex].Collapse(5);
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) ||
             (CurrentFigureIndex < 0))
                return;
            emblems[CurrentFigureIndex].Move(0, -10);
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) ||
             (CurrentFigureIndex < 0))
                return;
            emblems[CurrentFigureIndex].Move(0, 10);
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) ||
             (CurrentFigureIndex < 0))
                return;
            emblems[CurrentFigureIndex].Move(10, 0);
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) ||
             (CurrentFigureIndex < 0))
                return;
            emblems[CurrentFigureIndex].Move(-10, 0);
        }

        private void btnRightFar_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) ||
             (CurrentFigureIndex < 0))
                return;
            for (int i = 0; i < 100; i++)
            {
                emblems[CurrentFigureIndex].Move(1, 0);
                System.Threading.Thread.Sleep(5);
            }
        }

        private void btnLeftFar_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) ||
             (CurrentFigureIndex < 0))
                return;
            for (int i = 0; i < 100; i++)
            {
                emblems[CurrentFigureIndex].Move(-1, 0);
                System.Threading.Thread.Sleep(5);
            }
        }

        private void btnUpFar_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) ||
             (CurrentFigureIndex < 0))
                return;
            for (int i = 0; i < 100; i++)
            {
                emblems[CurrentFigureIndex].Move(0, -1);
                System.Threading.Thread.Sleep(5);
            }
        }

        private void btnDownFar_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) ||
             (CurrentFigureIndex < 0))
                return;
            for (int i = 0; i < 100; i++)
            {
                emblems[CurrentFigureIndex].Move(0, 1);
                System.Threading.Thread.Sleep(5);
            }
        }
    }
}
