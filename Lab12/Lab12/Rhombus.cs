﻿using System.Drawing;
class Rhombus : CEmblem
{
    // Поля
    private int _radius;
    // Властивості
    public int Radius
    {
        get
        {
            return _radius;
        }
        set
        {
            _radius = value >= 200 ? 200 : value;
            _radius = value <= 5 ? 5 : value;
        }
    }
    // Конструктор, створює об'єкт прямокутника (для заданої поверхні
    // малювання GDI+) з заданими координатами та довжиною сторін
    public Rhombus(Graphics graphics, int X, int Y, int Radius)
    {
        this.graphics = graphics;
        this.X = X; 
        this.Y = Y;
        this.Radius = Radius;
    }
    // Малює прямокутник на поверхні малювання GDI+
    protected override void Draw(Pen pen)
    {
        graphics.TranslateTransform(+X, +Y);
        graphics.RotateTransform(45);
        graphics.TranslateTransform(-X, -Y);
        Rectangle rectangle = new Rectangle(X - Radius + 15,
        Y - Radius + 15, Radius + 20, Radius + 20);
        graphics.DrawRectangle(pen, rectangle);
        graphics.ResetTransform();
    }
    // Розширює прямокутник: збільшує довжину стрін на dX пікселів
    override public void Expand(int dX)
    {
        Hide();
        Radius = Radius - dX;
        Show();
    }
    // Стискає прямокутник: зменшує довжину сторін на dX пікселів
    override public void Collapse(int dX)
    {
        Hide();
        Radius = Radius - dX;
        Show();
    }
} // Кінець оголошення класу