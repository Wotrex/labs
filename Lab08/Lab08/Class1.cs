﻿using System;

public class Book
{
    public string Name;
    public string Author;
    public string Genre;
    public int Copies;
    public int Pages;
    public double Cost;
    public int Fans;
    public double GetYearIncomePerInhabitant()
    {
        return Copies * Cost;
    }
}
