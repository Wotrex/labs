﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace lab08
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }

        private void btnBook_Click(object sender, EventArgs e)
        {
           Book book = new Book();
           fBook fb = new fBook(book);
           if (fb.ShowDialog() == DialogResult.OK)
           {
               tbBooksInfo.Text += string.Format("Назва книги: {0}.\r\nАвтор: {1}. \r\nЖанр: {2}.\r\nКількість копій: {3}.\r\nКількість сторінок: {4} сторінок.\r\nЦіна: {5:0.00} грн. \r\nКількість фанатів: {6}.\r\nМожливий дохід: {7:0.00} грн. \r\n \r\n", book.Name, book.Author, book.Genre, book.Copies, book.Pages, book.Cost, book.Fans, book.GetYearIncomePerInhabitant());
           }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Припинити роботу застосунку?","Припинити роботу", MessageBoxButtons.OKCancel,MessageBoxIcon.Question) == DialogResult.OK)
                Application.Exit(); 
        }
    }
}
