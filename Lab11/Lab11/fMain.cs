﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab11
{
    public partial class fMain : Form
    {
        CEmblem[] objects;
        int ObjectCount = 0;
        int CurrentObjectIndex;
        public fMain()
        {
            InitializeComponent();
            objects = new CEmblem[100];
        }


        private void btnCreateNew_Click(object sender, EventArgs e)
        {
            if (ObjectCount >= 99)
            {
                MessageBox.Show("Досягнуто межі кількості об'єктів!");
                return;
            }
            Graphics graphics = pnMain.CreateGraphics();
            CurrentObjectIndex = ObjectCount;
            objects[CurrentObjectIndex] = new CEmblem(graphics, pnMain.Width / 2, pnMain.Height / 2, 50);
            objects[CurrentObjectIndex].Show();
            ObjectCount++;
            cbObject.Items.Add("Коло №" + (ObjectCount).ToString());
            cbObject.SelectedIndex = ObjectCount - 1;
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            CurrentObjectIndex = cbObject.SelectedIndex;
            if ((CurrentObjectIndex > ObjectCount) || (CurrentObjectIndex < 0))
                return;
            // ---------------------------------------------------------------
            // Приховування поточного об'єкта - екземпляра класу CCircle
            objects[CurrentObjectIndex].Hide();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            CurrentObjectIndex = cbObject.SelectedIndex;
            if ((CurrentObjectIndex > ObjectCount) || (CurrentObjectIndex < 0))
                return;
            // ---------------------------------------------------------------
            // Відображення поточного об'єкта
            objects[CurrentObjectIndex].Show();
        }


        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Закрити застосунок?", "Вихід з програми", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void bntUpFar_Click(object sender, EventArgs e)
        {
            CurrentObjectIndex = cbObject.SelectedIndex;
            if ((CurrentObjectIndex > ObjectCount) || (CurrentObjectIndex < 0))
                return;
            // ---------------------------------------------------------------
            // Переміщення поточного об'єкта вверх на більшу відстань
            for (int i = 0; i < 100; i++)
            {
                objects[CurrentObjectIndex].Move(0, -
               1); System.Threading.Thread.Sleep(5);
            }
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            CurrentObjectIndex = cbObject.SelectedIndex;
            if ((CurrentObjectIndex > ObjectCount) || (CurrentObjectIndex < 0))
                return;
            // ---------------------------------------------------------------
            // Переміщення вверх поточного об'єкта
            objects[CurrentObjectIndex].Move(0, -10);
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            CurrentObjectIndex = cbObject.SelectedIndex;
            if ((CurrentObjectIndex > ObjectCount) || (CurrentObjectIndex < 0))
                return;
            // ---------------------------------------------------------------
            // Переміщення вліво поточного об'єкта
            objects[CurrentObjectIndex].Move(-10, 0);
            // -------------------------------------
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            CurrentObjectIndex = cbObject.SelectedIndex;
            if ((CurrentObjectIndex > ObjectCount) || (CurrentObjectIndex < 0))
                return;
            // ---------------------------------------------------------------
            // Переміщення вправо поточного об'єкта
            objects[CurrentObjectIndex].Move(10, 0);
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            CurrentObjectIndex = cbObject.SelectedIndex;
            if ((CurrentObjectIndex > ObjectCount) || (CurrentObjectIndex < 0))
                return;
            // ---------------------------------------------------------------
            // Переміщення вниз поточного об'єкта
            objects[CurrentObjectIndex].Move(0, 10);
        }

        private void btnExpand_Click(object sender, EventArgs e)
        {
            CurrentObjectIndex = cbObject.SelectedIndex;
            if ((CurrentObjectIndex > ObjectCount) || (CurrentObjectIndex < 0))
                return;
            // ---------------------------------------------------------------
            // Розширення поточного об'єкта - екземпляра класу CCircle
            objects[CurrentObjectIndex].Expand(5);
        }

        private void btnCollapse_Click(object sender, EventArgs e)
        {
            CurrentObjectIndex = cbObject.SelectedIndex;
            if ((CurrentObjectIndex > ObjectCount) || (CurrentObjectIndex < 0))
                return;
            // ---------------------------------------------------------------
            // Стискання поточного об'єкта - екземпляра класу CCircle
            objects[CurrentObjectIndex].Collapse(5);
        }

        private void btnLeftFar_Click(object sender, EventArgs e)
        {
            CurrentObjectIndex = cbObject.SelectedIndex;
            if ((CurrentObjectIndex > ObjectCount) || (CurrentObjectIndex < 0))
                return;
            // ---------------------------------------------------------------
            // Переміщення поточного об'єкта вліво на більшу відстань
            for (int i = 0; i < 100; i++)
            {
                objects[CurrentObjectIndex].Move(-1, 0);
                System.Threading.Thread.Sleep(5);
            }


        }

        private void btnRightFar_Click(object sender, EventArgs e)
        {
            CurrentObjectIndex = cbObject.SelectedIndex;
            if ((CurrentObjectIndex > ObjectCount) || (CurrentObjectIndex < 0))
                return;
            // ---------------------------------------------------------------
            // Переміщення поточного об'єкта вліво на більшу відстань
            for (int i = 0; i < 100; i++)
            {
                objects[CurrentObjectIndex].Move(1, 0);
                System.Threading.Thread.Sleep(5);
            }
        }

        private void btnDownFar_Click(object sender, EventArgs e)
        {
            CurrentObjectIndex = cbObject.SelectedIndex;
            if ((CurrentObjectIndex > ObjectCount) || (CurrentObjectIndex < 0))
                return;
            // ---------------------------------------------------------------
            // Переміщення поточного об'єкта вліво на більшу відстань
            for (int i = 0; i < 100; i++)
            {
                objects[CurrentObjectIndex].Move(0, 1);
                System.Threading.Thread.Sleep(5);
            }

        }
    }
}
