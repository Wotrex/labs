﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Lab14
{
    class Program
    {
        static List<Book> books;
        static void Main(string[] args)
        {
            books = new List<Book>();
            FileStream fs = new FileStream("Book.books", FileMode.Open);
            BinaryReader reader = new BinaryReader(fs);
            try
            {
                Book book;
                Console.WriteLine(" Читаємо данi з файлу...\n");
                while (reader.BaseStream.Position < reader.BaseStream.Length)
                {
                    book = new Book();
                    for (int i = 1; i <= 8; i++)
                    {
                        switch (i)
                        {
                            case 1:
                               book.Name = reader.ReadString();
                                break;
                            case 2:
                                book.Author = reader.ReadString();
                                break;
                            case 3:
                                book.Genre = reader.ReadString();
                                break;
                            case 4:
                                book.Copies = reader.ReadInt32();
                                break;
                            case 5:
                                book.Pages = reader.ReadInt32();
                                break;
                            case 6:
                                book.Cost = reader.ReadDouble();
                                break;
                            case 7:
                                book.Fans = reader.ReadInt32();
                                break;
                        }
                    }
                    books.Add(book);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Сталась помилка: {0}", ex.Message);
            }
            finally
            {
                reader.Close();
            }
            Console.WriteLine(" Несортований перелiк мiст: {0}",
            books.Count); PrintBooks(); books.Sort();
            Console.WriteLine(" Сортований перелiк мiст: {0}", books.Count);
            PrintBooks();
            Console.WriteLine(" Додаємо новий запис: Война і Мир");
            Book bookWarPeace = new Book("Война і Мир", "Л.Толстой", "Роман", 997000, 4000000, 237, 3245123);
            books.Add(bookWarPeace);
            books.Sort();
            Console.WriteLine(" Перелiк книг: {0}", books.Count);
            PrintBooks();
            Console.WriteLine(" Видаляємо останнє значення");
            books.RemoveAt(books.Count - 1);
            Console.WriteLine(" Перелiк мiст: {0}", books.Count);
            PrintBooks();
            Console.ReadKey(); 

        }
        static void PrintBooks()
        {
            foreach (Book book in books)
            {
                Console.WriteLine(book.Info().Replace('і', 'i'));
            }
            Console.WriteLine();
        }
    }
}
